package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

import edu.stanford.nlp.ling.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 13.12.16.
 */
public class NERString {
    public static final String NO_CATEGORY = "O";

    protected final List<NERToken> tokens;
    protected List<NERTokenGroup> groups;

    private void createGroups() {
        this.groups = new ArrayList<NERTokenGroup>();

        String lastCategory = null;
        List<NERToken> currGroupTokens = new ArrayList<NERToken>();
        for (NERToken nerToken : this.tokens) {

            if ((lastCategory != null) && (!nerToken.getCategory().equals(lastCategory))) {
                // add
                this.groups.add(new NERTokenGroup(currGroupTokens));
                currGroupTokens = new ArrayList<NERToken>();
            }

            currGroupTokens.add(nerToken);
            lastCategory = nerToken.getCategory();
        }

        // add
        this.groups.add(new NERTokenGroup(currGroupTokens));
    }

    public NERString(List<NERToken> tokens) {
        this.tokens = tokens;
        this.createGroups();
    }

    public List<NERToken> getTokens() {
        return tokens;
    }

    public List<NERTokenGroup> getGroups() {
        return groups;
    }

    public List<Word> getWords(int fromIndex, int toIndex) {
        return tokens.subList(fromIndex, toIndex).stream().map(t -> new Word(t.getText())).collect(Collectors.toList());
    }

    public List<Word> getWords() {
        return getWords(0, tokens.size());
    }

    @Override
    public String toString() {
        return tokens.stream().map(t -> t.toString()).collect(Collectors.joining("\n"));
    }
}
