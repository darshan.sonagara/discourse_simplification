package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DCore;

/**
 * Created by Matthias on 31.12.16.
 */
public class DCoreRelation {
    private final Relation relation;
    private final DCore dCore;

    public DCoreRelation(Relation relation, DCore dCore) {
        this.relation = relation;
        this.dCore = dCore;
    }

    public Relation getRelation() {
        return relation;
    }

    public DCore getDCore() {
        return dCore;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof DCoreRelation)
                && (((DCoreRelation) o).relation.equals(relation))
                && (((DCoreRelation) o).dCore.equals(dCore)));
    }
}
