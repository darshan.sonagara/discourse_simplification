package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

/**
 * Created by Matthias on 25.01.17.
 */
public class SentenceLeaf extends Leaf {

    public SentenceLeaf(String sentence, int sentenceIdx) {
        super(Type.DEFAULT, "SENTENCE", sentence);
        this.setRecursiveUnsetSentenceIdx(sentenceIdx);
    }
}
