package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.classification.SignalPhraseClassifier;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.CoordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SharedNPCoordinationExtractor extends ExtractionRule {
    private static final SignalPhraseClassifier CLASSIFIER = new SignalPhraseClassifier();

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: (S < (NP $.. (VP=vp < (VP $..VP))))");
        TregexMatcher matcher = p.matcher(parseTree);

        if (matcher.findAt(parseTree)) {
            List<Tree> siblings = getSiblings(matcher.getNode("vp"), Arrays.asList("VP"));

            // constituents
            List<Word> precedingWords = ParseTreeExtractionUtils.getPrecedingWords(parseTree, siblings.get(0), false);
            List<Word> followingWords = ParseTreeExtractionUtils.getFollowingWords(parseTree, siblings.get(siblings.size() - 1), false);
            List<List<Word>> constituentsWords = new ArrayList<List<Word>>();
            for (Tree sibling : siblings) {
                List<Word> constituentWords = new ArrayList<Word>();

                constituentWords.addAll(precedingWords);
                constituentWords.addAll(ParseTreeExtractionUtils.getContainingWords(sibling));
                constituentWords.addAll(followingWords);

                constituentsWords.add(constituentWords);
            }

            // result
            if (constituentsWords.size() == 2) {
                List<Word> signalPhraseWords = ParseTreeExtractionUtils.getWordsInBetween(parseTree, siblings.get(0), siblings.get(siblings.size() - 1), false, false);
                Optional<Relation> relation = CLASSIFIER.classifyGeneral(signalPhraseWords);

                Extraction res = new CoordinationExtraction(
                        getClass().getSimpleName(),
                        (relation.isPresent()) ? relation.get() : Relation.UNKNOWN_COORDINATION,
                        signalPhraseWords,
                        constituentsWords.get(0),
                        constituentsWords.get(constituentsWords.size() - 1),
                        Leaf.Type.DEFAULT
                );

                return Optional.of(res);
            } else {
                Extraction res = new CoordinationExtraction(
                        getClass().getSimpleName(),
                        Relation.UNKNOWN_COORDINATION,
                        constituentsWords,
                        Leaf.Type.DEFAULT
                );

                return Optional.of(res);
            }
        }

        return Optional.empty();
    }

}
