package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.classification.SignalPhraseClassifier;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.SubordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubordinationExtractor2 extends ExtractionRule {
    private static final SignalPhraseClassifier CLASSIFIER = new SignalPhraseClassifier();

    // custom mappings
    private static final List<SignalPhraseClassifier.Mapping> INTRA_SENTENTIAL_ATTRIBUTION_MAPPINGS = Arrays.asList(

            // INTRA_SENTENTIAL_ATTRIBUTION
            new SignalPhraseClassifier.Mapping(Relation.INTRA_SENTENTIAL_ATTRIBUTION, ""),
            new SignalPhraseClassifier.Mapping(Relation.INTRA_SENTENTIAL_ATTRIBUTION, "...that...")
    );

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: (S < (NP $.. (VP=vp <+(VP) (SBAR=sbar < (S=s)))))");
        TregexMatcher matcher = p.matcher(parseTree);

        if (matcher.findAt(parseTree)) {
            List<Word> signalPhraseWords = ParseTreeExtractionUtils.getPrecedingWords(matcher.getNode("sbar"), matcher.getNode("s"), false);

            // the left, (usually) superordinate constituent
            List<Word> leftConstituentWords = new ArrayList<Word>();
            leftConstituentWords.addAll(ParseTreeExtractionUtils.getPrecedingWords(parseTree, matcher.getNode("sbar"), false));
            leftConstituentWords.addAll(ParseTreeExtractionUtils.getFollowingWords(parseTree, matcher.getNode("sbar"), false));

            // the right, (usually) subordinate constituent
            List<Word> rightConstituentWords = ParseTreeExtractionUtils.getContainingWords(matcher.getNode("s"));

            // result
            Optional<Relation> relation = Optional.empty();
            boolean superordinationIsLeft = true;
            Leaf.Type leftConstituentType = Leaf.Type.DEFAULT;
            Leaf.Type rightConstituentType = Leaf.Type.DEFAULT;

            // intra sentential attribution
            if (!relation.isPresent()) {
                relation = CLASSIFIER.classifyCustom(INTRA_SENTENTIAL_ATTRIBUTION_MAPPINGS, signalPhraseWords);
                if (relation.isPresent()) {
                    leftConstituentWords = rephraseIntraSententialAttribution(leftConstituentWords);
                    leftConstituentType = Leaf.Type.SENT_SIM_CONTEXT;

                    // swap superordinate with subordinate assignment
                    superordinationIsLeft = !superordinationIsLeft;
                }
            }

            // enablement
            if (!relation.isPresent()) {
                if (isInfinitival(matcher.getNode("s"))) {
                    relation = Optional.of(Relation.ENABLEMENT);
                    rightConstituentWords = rephraseEnablement(matcher.getNode("s"), matcher.getNode("vp"));
                    rightConstituentType = Leaf.Type.SENT_SIM_CONTEXT;
                }
            }

            // general
            if (!relation.isPresent()) {
                relation = CLASSIFIER.classifyGeneral(signalPhraseWords);
            }

            Extraction res = new SubordinationExtraction(
                    getClass().getSimpleName(),
                    (relation.isPresent())? relation.get() : Relation.UNKNOWN_SUBORDINATION,
                    signalPhraseWords,
                    leftConstituentWords, // the superordinate constituent
                    rightConstituentWords, // the subordinate constituent
                    superordinationIsLeft,
                    leftConstituentType,
                    rightConstituentType);

            return Optional.of(res);
        }

        return Optional.empty();
    }

}
