package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Coordination;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.DiscourseTree;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 05.01.17.
 */
public class RefCoordinationExtraction extends Extraction {
    private final String extractionRule;
    private final Relation relation;
    private final String signalPhrase; // optional
    private final String rightCoordination;
    private final Leaf.Type rightCoordinationType;

    // binary
    public RefCoordinationExtraction(String extractionRule, Relation relation, List<Word> signalPhraseWords, List<Word> rightCoordinationWords, Leaf.Type rightCoordinationType) {
        this.extractionRule = extractionRule;
        this.relation = relation;
        this.signalPhrase = (signalPhraseWords != null)? WordsUtils.wordsToString(signalPhraseWords) : null;
        this.rightCoordination = WordsUtils.wordsToProperSentenceString(rightCoordinationWords);
        this.rightCoordinationType = rightCoordinationType;
    }

    public Optional<DiscourseTree> convert(Leaf currChild) {

        // find previous node to use as a reference
        Optional<DiscourseTree> prevNode = currChild.getPreviousNode();
        if ((prevNode.isPresent()) && (prevNode.get().usableAsReference())) {

            // use prev node as a reference
            prevNode.get().useAsReference();

            Coordination res = new Coordination(
                    extractionRule,
                    relation,
                    signalPhrase,
                    Arrays.asList()
            );
            res.addCoordination(prevNode.get()); // set prev node as a reference
            res.addCoordination(new Leaf(rightCoordinationType, extractionRule, rightCoordination));

            return Optional.of(res);

        }

        return Optional.empty();
    }

}
