package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthias on 30.12.16.
 */
public class Leaf extends DiscourseTree {
    public enum Type {
        DEFAULT, // can be splitted
        TERMINAL, // will not be splitted
        SENT_SIM_CONTEXT // will not be splitted and will act like a sentence-simplification content in Step 3
    }

    private final Type type;
    private final String text;

    public Leaf(Type type, String extractionRule, String text) {
        super(extractionRule);
        this.type = type;
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    @Override
    public List<String> getPTPCaption() {
        String typeStr = "";
        if (type.equals(Type.TERMINAL)) {
            typeStr = " [terminal]";
        } else if (type.equals(Type.SENT_SIM_CONTEXT)) {
            typeStr = " [s-context]";
        }

        return Arrays.asList("'" + text + "'" + typeStr);
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        return new ArrayList<PrettyTreePrinter.Edge>();
    }

}
