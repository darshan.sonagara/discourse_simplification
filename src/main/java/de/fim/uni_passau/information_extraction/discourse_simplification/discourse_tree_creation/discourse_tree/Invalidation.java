package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthias on 05.01.17.
 */
public class Invalidation extends DiscourseTree {

    public Invalidation() {
        super("");
    }

    @Override
    public List<String> getPTPCaption() {
        return Arrays.asList("INVALIDATED");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        return new ArrayList<PrettyTreePrinter.Edge>();
    }
}
