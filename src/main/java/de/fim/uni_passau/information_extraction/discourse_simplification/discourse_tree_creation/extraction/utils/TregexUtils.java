package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.utils;

import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Matthias on 23.01.17.
 */
public class TregexUtils {

    public static class MyMatch {
        public static class Comparator implements java.util.Comparator<MyMatch> {
            private final Tree anchorTree;
            private final List<String> names;

            public Comparator(Tree anchorTree, List<String> names) {
                this.anchorTree = anchorTree;
                this.names = names;
            }

            @Override
            public int compare(MyMatch myMatch, MyMatch otherMatch) {
                int myMatchValue = 0;
                int otherMatchValue = 0;
                for (String name : names) {
                    myMatchValue += myMatch.getNode(name).nodeNumber(anchorTree);
                    otherMatchValue += otherMatch.getNode(name).nodeNumber(anchorTree);
                }

                return myMatchValue - otherMatchValue;
            }
        }

        private final HashMap<String, Tree> groups;

        public MyMatch(HashMap<String, Tree> groups) {
            this.groups = groups;
        }

        public Tree getNode(String name) {
            if (groups.containsKey(name)) {
                return groups.get(name);
            } else {
                throw new IllegalArgumentException("No tree for name: '" + name + "'");
            }
        }
    }

    public static List<MyMatch> sortedFindAt(Tree parseTree, TregexPattern p, List<String> groupsToOrder) {
        List<MyMatch> res = new ArrayList<MyMatch>();

        TregexMatcher matcher = p.matcher(parseTree);
        while (matcher.findAt(parseTree)) {
            HashMap<String, Tree> groups = new HashMap<>();
            for (String name : matcher.getNodeNames()) {
                groups.put(name, matcher.getNode(name));
            }
            res.add(new MyMatch(groups));
        }

        // sort groups
        res.sort(new MyMatch.Comparator(parseTree, groupsToOrder));

        return res;
    }

    public static List<MyMatch> sortedFind(Tree parseTree, TregexPattern p, List<String> groupsToOrder) {
        List<MyMatch> res = new ArrayList<MyMatch>();

        TregexMatcher matcher = p.matcher(parseTree);
        while (matcher.find()) {
            HashMap<String, Tree> groups = new HashMap<>();
            for (String name : matcher.getNodeNames()) {
                groups.put(name, matcher.getNode(name));
            }
            res.add(new MyMatch(groups));
        }

        // sort groups
        res.sort(new MyMatch.Comparator(parseTree, groupsToOrder));

        return res;
    }
}
