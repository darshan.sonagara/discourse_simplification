package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.tner;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.NERToken;
import edu.stanford.nlp.trees.Tree;

/**
 * Created by Matthias on 16.12.16.
 */
public class TNERToken extends NERToken {

    private TNERString nerString;
    private final Tree leafNode;
    private Tree posNode;

    public TNERToken(int index, String token, String category, Tree leafNode) {
        super(index, token, category);
        this.nerString = null;
        this.leafNode = leafNode;
        this.posNode = null; // wait until nerString is set
    }

    public void setNerString(TNERString nerString) {
        this.nerString = nerString;
        this.posNode = leafNode.parent(getParseTree());
    }

    public Tree getParseTree() {
        return nerString.getParseTree();
    }

    public Tree getLeafNode() {
        return leafNode;
    }

    public Tree getPosNode() {
        return posNode;
    }

    public String getPOSTag() {
        return posNode.value();
    }

    @Override
    public String toString() {
        return "(" + index + ": " + category +  ", '" + text + "', " + getPOSTag() + ")";
    }
}
