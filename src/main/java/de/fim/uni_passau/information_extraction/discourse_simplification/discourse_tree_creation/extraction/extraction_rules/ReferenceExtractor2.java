package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.classification.SignalPhraseClassifier;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.RefCoordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 04.01.17.
 */
public class ReferenceExtractor2 extends ExtractionRule {
    private static final SignalPhraseClassifier CLASSIFIER = new SignalPhraseClassifier();

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: S <<, (__=node >1 S << /this|that/=det)");
        TregexMatcher matcher = p.matcher(parseTree);

        if (matcher.findAt(parseTree)) {
            List<Word> signalPhraseWords = ParseTreeExtractionUtils.getPrecedingWords(matcher.getNode("node"), matcher.getNode("det"), true);

            // the right constituent
            List<Word> rightConstituentWords = ParseTreeExtractionUtils.getFollowingWords(parseTree, matcher.getNode("node"), false);

            // result
            Optional<Relation> relation = CLASSIFIER.classifyGeneral(signalPhraseWords);
            if (relation.isPresent()) {
                Extraction res = new RefCoordinationExtraction(
                        getClass().getSimpleName(),
                        relation.get(),
                        signalPhraseWords,
                        rightConstituentWords,
                        Leaf.Type.DEFAULT
                );

                return Optional.of(res);
            }
        }

        return Optional.empty();
    }
}
