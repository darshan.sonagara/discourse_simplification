package de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree;

/**
 * Created by Matthias on 13.12.16.
 */
public class ParseTreeException extends Exception {

    public ParseTreeException(String text) {
        super("Failed to parse text: \"" + text + "\"");
    }
}
