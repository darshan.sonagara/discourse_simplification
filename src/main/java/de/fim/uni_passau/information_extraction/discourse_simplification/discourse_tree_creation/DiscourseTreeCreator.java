package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.*;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.CoordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.RefCoordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.RefSubordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.SubordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules.*;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeException;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeParser;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeVisualizer;
import edu.stanford.nlp.trees.Tree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by Matthias on 09.12.16.
 */
public class DiscourseTreeCreator {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String ROOT_EXTRACTION_RULE = "ROOT";

    private static List<ExtractionRule> rules;
    static {
        rules = new ArrayList<ExtractionRule>();

        rules.add(new ReferenceExtractor1());
        rules.add(new ReferenceExtractor2());
        rules.add(new CoordinationExtractor());
        rules.add(new SharedNPCoordinationExtractor());
        rules.add(new SubordinationExtractor1());
        rules.add(new SubordinationExtractor2());
        rules.add(new Subordination3EnablementExtractor());
        rules.add(new Subordination4EnablementExtractor());

        rules.add(new ListNPExtractor1());
        rules.add(new ListNPExtractor2());
    }

    private Coordination discourseTree;

    public void reset() {
        this.discourseTree = new Coordination(
                ROOT_EXTRACTION_RULE,
                Relation.UNKNOWN_COORDINATION,
                null,
                new ArrayList<DiscourseTree>()

        );
    }

    public DiscourseTreeCreator() {
        reset();
    }

    public void addSentence(String sentence, int sentenceIdx) {
        discourseTree.addCoordination(new SentenceLeaf(sentence, sentenceIdx));
    }

    public Optional<DiscourseTree> getLastSentenceTree() {
        DiscourseTree res = null;
        if (discourseTree.getCoordinations().size() > 0) {
            res = discourseTree.getCoordinations().get(discourseTree.getCoordinations().size() - 1);
        }

        return Optional.ofNullable(res);
    }

    public Coordination getDiscourseTree() {
        return discourseTree;
    }

    public void update() {
        processDiscourseTreeRec(discourseTree);
        discourseTree.cleanup();
    }

    private void processDiscourseTreeRec(DiscourseTree discourseTree) {

        if (discourseTree instanceof Coordination) {
            Coordination coordination = (Coordination)discourseTree;

            for (DiscourseTree child : coordination.getCoordinations()) {

                // process coordination-leaf if not processed yet
                if (!child.isProcessed()) {
                    DiscourseTree c = child;

                    if (child instanceof Leaf) {
                        Optional<DiscourseTree> newChild = applyRules((Leaf) child);
                        if (newChild.isPresent()) {
                            coordination.replaceCoordination(child, newChild.get());
                            c = newChild.get();
                        }
                    }

                    child.setProcessed();

                    // recursion
                    processDiscourseTreeRec(c);
                }
            }
        }

        if (discourseTree instanceof Subordination) {
            Subordination subordination = (Subordination)discourseTree;

            // process superordination-leaf if not processed yet
            if (!subordination.getSuperordination().isProcessed()) {

                if (subordination.getSuperordination() instanceof Leaf) {
                    Optional<DiscourseTree> newChild = applyRules((Leaf) subordination.getSuperordination());
                    if (newChild.isPresent()) {
                        subordination.replaceSuperordination(newChild.get());
                    }
                }

                subordination.getSuperordination().setProcessed();

                // recursion
                processDiscourseTreeRec(subordination.getSuperordination());
            }

            // process subordination-leaf if not processed yet
            if (!subordination.getSubordination().isProcessed()) {

                if (subordination.getSubordination() instanceof Leaf) {
                    Optional<DiscourseTree> newChild = applyRules((Leaf) subordination.getSubordination());
                    if (newChild.isPresent()) {
                        subordination.replaceSubordination(newChild.get());
                    }
                }

                subordination.getSubordination().setProcessed();

                // recursion
                processDiscourseTreeRec(subordination.getSubordination());
            }
        }
    }

    private Optional<DiscourseTree> applyRules(Leaf leaf) {
        logger.debug("Processing leaf:");
        if (logger.isDebugEnabled()) {
            logger.debug(leaf.toString());
        }

        if ((leaf.getType().equals(Leaf.Type.TERMINAL)) || (leaf.getType().equals(Leaf.Type.SENT_SIM_CONTEXT))) {
            logger.debug("Leaf will not be splitted.");
            return Optional.empty();
        }

        // try to generate parseTree
        Tree parseTree;
        try {
            parseTree = ParseTreeParser.parse(leaf.getText());
        } catch (ParseTreeException e) {
            logger.error("Failed to generate parse tree");

            return Optional.empty();
        }
        logger.debug("Parse tree:");
        if (logger.isDebugEnabled()) {
            logger.debug(ParseTreeVisualizer.prettyPrint(parseTree));
        }

        // check rules
        for (ExtractionRule rule : rules) {

            Optional<Extraction> extraction = rule.extract(parseTree);
            if (extraction.isPresent()) {
                logger.debug("Extraction rule " + rule.getClass().getSimpleName() + " matched.");

                // handle CoordinationExtraction
                if (extraction.get() instanceof CoordinationExtraction) {
                    return Optional.of(((CoordinationExtraction)extraction.get()).convert());
                }

                // handle SubordinationExtraction
                if (extraction.get() instanceof SubordinationExtraction) {
                    return Optional.of(((SubordinationExtraction)extraction.get()).convert());
                }

                // handle RefCoordinationExtraction
                if (extraction.get() instanceof RefCoordinationExtraction) {
                    Optional<DiscourseTree> r = ((RefCoordinationExtraction)extraction.get()).convert(leaf);
                    if (r.isPresent()) {
                        return r;
                    } else {
                        logger.debug("Reference could not be used, checking other extraction rules.");
                    }
                }

                // handle RefSubordinationExtraction
                if (extraction.get() instanceof RefSubordinationExtraction) {
                    Optional<DiscourseTree> r = ((RefSubordinationExtraction)extraction.get()).convert(leaf);
                    if (r.isPresent()) {
                        return r;
                    } else {
                        logger.debug("Reference could not be used, checking other extraction rules.");
                    }
                }
            }
        }
        logger.debug("No extraction rule applied.");

        return Optional.empty();
    }

}
