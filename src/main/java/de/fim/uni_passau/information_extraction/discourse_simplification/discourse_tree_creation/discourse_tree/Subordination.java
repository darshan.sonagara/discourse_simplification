package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthias on 30.12.16.
 */
public class Subordination extends DiscourseTree {
    protected final Relation relation;
    private final String signalPhrase; // optional
    private DiscourseTree leftConstituent;
    private DiscourseTree rightConstituent;
    private final boolean superordinationIsLeft;

    public Subordination(String extractionRule, Relation relation, String signalPhrase, DiscourseTree leftConstituent, DiscourseTree rightConstituent, boolean superordinationIsLeft) {
        super(extractionRule);
        this.relation = relation;
        this.signalPhrase = signalPhrase;
        this.superordinationIsLeft = superordinationIsLeft;

        this.leftConstituent = new Leaf(Leaf.Type.DEFAULT, "tmp", "tmp");
        this.rightConstituent = new Leaf(Leaf.Type.DEFAULT, "tmp", "tmp");
        replaceLeftConstituent(leftConstituent);
        replaceRightConstituent(rightConstituent);
    }

    public void replaceLeftConstituent(DiscourseTree newLeftConstituent) {
        DiscourseTree oldLeftConstituent = this.leftConstituent;
        this.leftConstituent = newLeftConstituent;
        newLeftConstituent.parent = this;
        newLeftConstituent.setRecursiveUnsetSentenceIdx(oldLeftConstituent.getSentenceIdx());
    }

    public void replaceRightConstituent(DiscourseTree newRightConstituent) {
        DiscourseTree oldRightConstituent = this.rightConstituent;
        this.rightConstituent = newRightConstituent;
        newRightConstituent.parent = this;
        newRightConstituent.setRecursiveUnsetSentenceIdx(oldRightConstituent.getSentenceIdx());
    }

    public void replaceSuperordination(DiscourseTree newSuperordination) {
        if (superordinationIsLeft) {
            replaceLeftConstituent(newSuperordination);
        } else {
            replaceRightConstituent(newSuperordination);
        }
    }

    public void replaceSubordination(DiscourseTree newSubordination) {
        if (superordinationIsLeft) {
            replaceRightConstituent(newSubordination);
        } else {
            replaceLeftConstituent(newSubordination);
        }
    }

    public Relation getRelation() {
        return relation;
    }

    public DiscourseTree getLeftConstituent() {
        return leftConstituent;
    }

    public DiscourseTree getRightConstituent() {
        return rightConstituent;
    }

    public DiscourseTree getSuperordination() {
        return (superordinationIsLeft)? leftConstituent : rightConstituent;
    }

    public DiscourseTree getSubordination() {
        return (superordinationIsLeft)? rightConstituent : leftConstituent;
    }

    @Override
    public List<String> getPTPCaption() {
        String signalPhraseStr = (signalPhrase != null)? "'" + signalPhrase + "'" : "NULL";
        return Arrays.asList("SUB/" + relation + " (" + signalPhraseStr + ", " + extractionRule + ")");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        List<PrettyTreePrinter.Edge> res = new ArrayList<PrettyTreePrinter.Edge>();
        res.add(new PrettyTreePrinter.DefaultEdge((superordinationIsLeft)? "n" : "s", leftConstituent, true));
        res.add(new PrettyTreePrinter.DefaultEdge((superordinationIsLeft)? "s" : "n", rightConstituent, true));

        return res;
    }

}
