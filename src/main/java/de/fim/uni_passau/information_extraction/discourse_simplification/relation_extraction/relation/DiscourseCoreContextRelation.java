package de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element.DiscourseContext;

/**
 * Created by Matthias on 31.12.16.
 */
public class DiscourseCoreContextRelation {
    private final Relation relation;
    private final DiscourseContext context;

    public DiscourseCoreContextRelation(Relation relation, DiscourseContext context) {
        this.relation = relation;
        this.context = context;
    }

    public Relation getRelation() {
        return relation;
    }

    public DiscourseContext getContext() {
        return context;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof DiscourseCoreContextRelation)
                    && (((DiscourseCoreContextRelation) o).relation.equals(relation))
                        && (((DiscourseCoreContextRelation) o).context.equals(context)));
    }
}
