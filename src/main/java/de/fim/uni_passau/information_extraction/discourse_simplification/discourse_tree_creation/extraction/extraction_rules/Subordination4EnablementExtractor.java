package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.SubordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 06.01.17.
 */
public class Subordination4EnablementExtractor extends ExtractionRule {

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: (S < (S=s $.. (NP $.. VP=vp)))");
        TregexMatcher matcher = p.matcher(parseTree);

        if (matcher.findAt(parseTree)) {

            // the left, subordinate constituent
            List<Word> leftConstituentWords = ParseTreeExtractionUtils.getContainingWords(matcher.getNode("s"));

            // the right, superordinate constituent
            List<Word> rightConstituentWords = new ArrayList<Word>();
            rightConstituentWords.addAll(ParseTreeExtractionUtils.getPrecedingWords(parseTree, matcher.getNode("s"), false));
            rightConstituentWords.addAll(ParseTreeExtractionUtils.getFollowingWords(parseTree, matcher.getNode("s"), false));

            // result
            Optional<Relation> relation = Optional.empty();
            Leaf.Type leftConstituentType = Leaf.Type.DEFAULT;
            Leaf.Type rightConstituentType = Leaf.Type.DEFAULT;

            // enablement
            if (!relation.isPresent()) {
                if (isInfinitival(matcher.getNode("s"))) {
                    relation = Optional.of(Relation.ENABLEMENT);
                    leftConstituentWords = rephraseEnablement(matcher.getNode("s"), matcher.getNode("vp"));
                    leftConstituentType = Leaf.Type.SENT_SIM_CONTEXT;
                }
            }

            if (relation.isPresent()) {
                Extraction res = new SubordinationExtraction(
                        getClass().getSimpleName(),
                        relation.get(),
                        null,
                        leftConstituentWords, // the subordinate constituent
                        rightConstituentWords, // the superordinate constituent
                        false,
                        leftConstituentType,
                        rightConstituentType
                );

                return Optional.of(res);
            }
        }

        return Optional.empty();
    }
}
