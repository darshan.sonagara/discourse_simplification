package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.utils;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 23.01.17.
 */
public class ListNPSplitter {

    public static class Result {
        private final List<List<Word>> elementsWords;
        private final Relation relation;

        public Result(List<List<Word>> elementsWords, Relation relation) {
            this.elementsWords = elementsWords;
            this.relation = relation;
        }

        public List<List<Word>> getElementsWords() {
            return elementsWords;
        }

        public Relation getRelation() {
            return relation;
        }
    }


    public static Optional<Result> split(Tree np) {

        // representation
        String representation = np.getChildrenAsList().stream().map(c -> (c.value().equals("CC"))? c.yieldWords().get(0).value() : c.value()).collect(Collectors.joining(""));

        final String LIST_CONJUNCTION_PATTERN = "^(NP|,)*NP(NP|,)*(and(NP|,)*NP(NP|,)*)+$";
        final String LIST_DISJUNCTION_PATTERN = "^(NP|,)*NP(NP|,)*(or(NP|,)*NP(NP|,)*)+$";
        if (representation.matches(LIST_CONJUNCTION_PATTERN) || representation.matches(LIST_DISJUNCTION_PATTERN)) {
            Relation relation = representation.matches(LIST_CONJUNCTION_PATTERN)? Relation.JOINT_NP_LIST : Relation.JOINT_NP_DISJUNCTION;

            // get last CC index
            int lastCCIdx = 0;
            for (int i = np.getChildrenAsList().size() - 1; i >= 0; i--) {
                Tree child = np.getChildrenAsList().get(i);
                if (child.value().equals("CC")) {
                    lastCCIdx = i;
                    break;
                }
            }

            // extract
            List<List<Word>> elementsWords = new ArrayList<List<Word>>();
            boolean foundFirstNPAfterCC = false;
            for (int i = 0; i < np.getChildrenAsList().size(); i++) {
                Tree child = np.getChildrenAsList().get(i);

                if (foundFirstNPAfterCC) {
                    elementsWords.get(elementsWords.size() - 1).addAll(child.yieldWords());
                } else if (child.value().equals("NP")) {
                    elementsWords.add(child.yieldWords());
                    if (i > lastCCIdx) {
                        foundFirstNPAfterCC = true;
                    }
                }
            }

            return Optional.of(new Result(elementsWords, relation));
        }

        return Optional.empty();
    }

}
