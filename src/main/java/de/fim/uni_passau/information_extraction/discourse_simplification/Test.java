package de.fim.uni_passau.information_extraction.discourse_simplification;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeException;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeParser;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeVisualizer;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.sentences.SentencesUtils;
import edu.stanford.nlp.trees.Tree;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Matthias on 06.01.17.
 */
public class Test {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void printParseTree(File file) throws FileNotFoundException {
        List<String> sentences = SentencesUtils.splitIntoSentencesFromFile(file);

        for (String sentence : sentences) {
            LOGGER.info("Generate parse tree for sentence:\n'{}'", sentence);
            try {
                Tree parseTree = ParseTreeParser.parse(sentence);
                LOGGER.info(ParseTreeVisualizer.prettyPrint(parseTree));
            } catch (ParseTreeException e) {
                LOGGER.error("Failed to generate parse tree");
            }
        }
    }

    public static void printParseTree(String text) throws FileNotFoundException {
        List<String> sentences = SentencesUtils.splitIntoSentences(text);

        for (String sentence : sentences) {
            LOGGER.info("Generate parse tree for sentence:\n'{}'", sentence);
            try {
                Tree parseTree = ParseTreeParser.parse(sentence);
                LOGGER.info(ParseTreeVisualizer.prettyPrint(parseTree));
            } catch (ParseTreeException e) {
                LOGGER.error("Failed to generate parse tree");
            }
        }
    }

}
