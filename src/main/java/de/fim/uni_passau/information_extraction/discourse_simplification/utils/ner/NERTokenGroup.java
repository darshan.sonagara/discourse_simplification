package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

import edu.stanford.nlp.ling.Word;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 16.12.16.
 */
public class NERTokenGroup {
    protected final List<NERToken> tokens;

    public NERTokenGroup(List<NERToken> tokens) {
        this.tokens = tokens;
    }

    public int getFromTokenIndex() {
        return tokens.get(0).index;
    }

    public int getToTokenIndex() {
        return tokens.get(tokens.size() - 1).index;
    }

    public List<NERToken> getTokens() {
        return tokens;
    }

    public String getCategory() {
        return tokens.get(0).getCategory();
    }

    public boolean isNamedEntity() {
        return !getCategory().equals(NERString.NO_CATEGORY);
    }

    public List<Word> getWords() {
        return tokens.stream().map(t -> new Word(t.getText())).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "[\n" + tokens.stream().map(t -> "\t" + t.toString()).collect(Collectors.joining("\n")) + "\n]";
    }
}
