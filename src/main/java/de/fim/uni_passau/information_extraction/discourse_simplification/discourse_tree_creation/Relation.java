package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Relation {

    // default relations
    UNKNOWN_COORDINATION, // the default for coordination
    UNKNOWN_SUBORDINATION, // the default for subordination
    UNKNOWN_SENT_SIM, // the default for sentence simplification

    BACKGROUND,
    CAUSE,
    CONDITION,
    CONTRAST,
    ELABORATION,
    ENABLEMENT,
    EXPLANATION,
    JOINT_LIST,
    JOINT_DISJUNCTION,
    TEMPORAL_BEFORE,
    TEMPORAL_AFTER,
    TEMPORAL_SEQUENCE,

    // special relations
    INTRA_SENTENTIAL_ATTRIBUTION,
    JOINT_NP_LIST,
    JOINT_NP_DISJUNCTION,

    // sentence simplification
    TIME,
    LOCATION;

    private Relation reverseRelation;
    static {
        TEMPORAL_AFTER.reverseRelation = TEMPORAL_BEFORE;
        TEMPORAL_BEFORE.reverseRelation = TEMPORAL_AFTER;
    }

    private Relation() {
        /*
         * by default, each relation is bidirectional with an equal reverse relation.
         * To make a relation unidirectional, set reverseRelation to null.
         */
        this.reverseRelation = this;
    }


    public Optional<Relation> getReverseRelation() {
        return Optional.ofNullable(reverseRelation);
    }
}
