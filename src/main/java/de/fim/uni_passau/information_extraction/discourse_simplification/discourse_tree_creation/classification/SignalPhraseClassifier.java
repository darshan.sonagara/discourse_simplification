package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.classification;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 09.01.17.
 */
public class SignalPhraseClassifier {

    public static class Mapping {
        private final Relation relation;

        private final String signalPhrasePattern; // optional
        private final int signalPhrasePatternSize;

        public Mapping(Relation relation, String signalPhrasePattern) {
            this.relation = relation;
            this.signalPhrasePattern = "^" + signalPhrasePattern.replaceAll("\\.\\.\\.", "((?<=^)(.*\\\\W)?|\\\\W|\\\\W.*\\\\W|(\\\\W.*)?(?=\\$))") + "$";
            this.signalPhrasePatternSize = signalPhrasePattern.length();
        }

        public boolean check(String signalPhrase) {
            return signalPhrase.toLowerCase().matches(signalPhrasePattern);
        }

        public Relation getRelation() {
            return relation;
        }

        public int getSignalPhrasePatternSize() {
            return signalPhrasePatternSize;
        }
    }

    private static final List<Mapping> GENERAL_MAPPINGS = Arrays.asList(

            // BACKGROUND
            new Mapping(Relation.BACKGROUND, "...as..."),
            new Mapping(Relation.BACKGROUND, "...now..."),
            new Mapping(Relation.BACKGROUND, "...once..."),
//            new Mapping(Relation.BACKGROUND, "previously"),
            new Mapping(Relation.BACKGROUND, "...when..."),
            new Mapping(Relation.BACKGROUND, "...with..."),
            new Mapping(Relation.BACKGROUND, "...without..."),

            // CAUSE
            new Mapping(Relation.CAUSE, "...largely because..."),
            new Mapping(Relation.CAUSE, "...as a result..."),
            new Mapping(Relation.CAUSE, "...as a result of..."),
            new Mapping(Relation.CAUSE, "...because..."),
            new Mapping(Relation.CAUSE, "...since..."),

            // CONDITION
            new Mapping(Relation.CONDITION, "...if..."),
            new Mapping(Relation.CONDITION, "...in case..."),
            new Mapping(Relation.CONDITION, "...unless..."),
            new Mapping(Relation.CONDITION, "...until..."),

            // CONTRAST
            new Mapping(Relation.CONTRAST, "...although..."),
            new Mapping(Relation.CONTRAST, "...but..."),
            new Mapping(Relation.CONTRAST, "...but now..."),
            new Mapping(Relation.CONTRAST, "...despite..."),
            new Mapping(Relation.CONTRAST, "...even though..."),
            new Mapping(Relation.CONTRAST, "...even when..."),
            new Mapping(Relation.CONTRAST, "...however..."),
            new Mapping(Relation.CONTRAST, "...instead..."),
            new Mapping(Relation.CONTRAST, "...rather..."),
            new Mapping(Relation.CONTRAST, "...still..."),
            new Mapping(Relation.CONTRAST, "...though..."),
            new Mapping(Relation.CONTRAST, "...thus..."),
            new Mapping(Relation.CONTRAST, "...until recently..."),
            new Mapping(Relation.CONTRAST, "...while..."),
            new Mapping(Relation.CONTRAST, "...yet..."),

            // ELABORATION
            new Mapping(Relation.ELABORATION, "...more provocatively..."),
            new Mapping(Relation.ELABORATION, "...even before..."),
            new Mapping(Relation.ELABORATION, "...for example..."),
            new Mapping(Relation.ELABORATION, "...further..."),
            new Mapping(Relation.ELABORATION, "...recently..."),
            new Mapping(Relation.ELABORATION, "...since...now..."),
            new Mapping(Relation.ELABORATION, "...so..."),
            new Mapping(Relation.ELABORATION, "...so far..."),
            new Mapping(Relation.ELABORATION, "...where..."),
            new Mapping(Relation.ELABORATION, "...whereby..."),
            new Mapping(Relation.ELABORATION, "...whether..."),

            // EXPLANATION
            new Mapping(Relation.EXPLANATION, "...simply because..."),
            new Mapping(Relation.EXPLANATION, "...because of..."),
            new Mapping(Relation.EXPLANATION, "...indeed..."),
            new Mapping(Relation.EXPLANATION, "...so...that..."),

            // JOINT_LIST
            new Mapping(Relation.JOINT_LIST, "...and..."),

            // JOINT_DISJUNCTION
            new Mapping(Relation.JOINT_DISJUNCTION, "...or..."),

            // TEMPORAL_BEFORE
            new Mapping(Relation.TEMPORAL_BEFORE, "...before..."),
            new Mapping(Relation.TEMPORAL_BEFORE, "...previously..."), // changed from BACKGROUND TO TEMPORAL_BEFORE

            // TEMPORAL_AFTER
            new Mapping(Relation.TEMPORAL_AFTER, "...after..."),
            new Mapping(Relation.TEMPORAL_AFTER, "...and after..."),
            new Mapping(Relation.TEMPORAL_AFTER, "...next..."),
            new Mapping(Relation.TEMPORAL_AFTER, "...then..."),

            // TEMPORAL_SEQUENCE
            new Mapping(Relation.TEMPORAL_SEQUENCE, "...thereafter...")
    );

    private Optional<Relation> classify(List<Mapping> mappings, List<Word> signalPhraseWords) {
        String signalPhrase = WordsUtils.wordsToString(signalPhraseWords);

        Mapping bestMapping = null;
        for (Mapping mapping : mappings) {
            if (mapping.check(signalPhrase)) {
                if ((bestMapping == null) || (mapping.getSignalPhrasePatternSize() >= bestMapping.getSignalPhrasePatternSize())) {
                    bestMapping = mapping;
                }
            }
        }
        if (bestMapping != null) {
            return Optional.of(bestMapping.getRelation());
        } else {
            return Optional.empty();
        }
    }

    public Optional<Relation> classifyCustom(List<Mapping> mappings, List<Word> signalPhraseWords) {
        return classify(mappings, signalPhraseWords);
    }

    public Optional<Relation> classifyGeneral(List<Word> signalPhraseWords) {
        return classify(GENERAL_MAPPINGS, signalPhraseWords);
    }
}
