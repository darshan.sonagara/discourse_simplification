package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element.DiscourseContext;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element.DiscourseCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation.DiscourseCoreContextRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation.DiscourseCoreCoreRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.classification.SContextClassifier;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.SContext;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DContext;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation.DContextRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation.DCoreRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.trees.Tree;
import org.lamba3.simplification.sentence_simplification.transformation.CoreContextSentence;
import org.lamba3.simplification.sentence_simplification.transformation.SentenceSimplifyingException;
import org.lamba3.simplification.sentence_simplification.transformation.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 03.01.17.
 */
public class Simplifier {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private LinkedHashMap<DiscourseCore, DCore> processedDiscourseCores;
    private LinkedHashMap<DiscourseContext, DContext> processedDiscourseContexts;

    public Simplifier() {
        this.processedDiscourseCores = new LinkedHashMap<DiscourseCore, DCore>();
        this.processedDiscourseContexts = new LinkedHashMap<DiscourseContext, DContext>();
    }

    private static SContext createSContext(String text, int sentenceIdx) {
        Optional<Relation> relation = SContextClassifier.classify(text);
        if (relation.isPresent()) {
            return new SContext(text, sentenceIdx, relation.get());
        } else {
            return new SContext(text, sentenceIdx, Relation.UNKNOWN_SENT_SIM);
        }
    }

    private DContext getDContext(DiscourseContext discourseContext) {
        DContext res;

        if (processedDiscourseContexts.containsKey(discourseContext)) {
            res = processedDiscourseContexts.get(discourseContext);
        } else {
            String text = discourseContext.getText();
            List<SContext> sentSimContexts = new ArrayList<SContext>();

            // apply sentence simplification
            Transformer t = new Transformer();
            try {
                logger.debug("Simplifying: '{}'", discourseContext.getText());
                CoreContextSentence s = t.simplify(discourseContext.getText());

                // set coreText (assume that there is usually only one core)
                if ((s.getCore() != null) && (s.getCore().size() > 0)) {
                    Tree c = s.getCore().get(0);
                    if (c != null) {
                        text = WordsUtils.wordsToString(c.yieldWords());
                    }
                }

                // add (sentence simplification) contexts
                if (s.getContext() != null) {
                    for (Tree c : s.getContext()) {
                        if (c != null) {
                            sentSimContexts.add(createSContext(WordsUtils.wordsToString(c.yieldWords()), discourseContext.getSentenceIdx()));
                        }
                    }
                }
            } catch (SentenceSimplifyingException e) {
                // nothing
            }

            res = new DContext(text, discourseContext.getSentenceIdx(), discourseContext.getText());

            // add (sentence simplification) context relations
            for (SContext sentSimContext : sentSimContexts) {
                res.addSContext(sentSimContext);
            }

            processedDiscourseContexts.put(discourseContext, res);
        }

        return res;
    }

    private DCore getDCore(DiscourseCore discourseCore) {
        DCore res;

        if (processedDiscourseCores.containsKey(discourseCore)) {
            res = processedDiscourseCores.get(discourseCore);
        } else {
            String text = discourseCore.getText();
            List<SContext> sentSimContexts = new ArrayList<SContext>();

            // apply sentence simplification
            Transformer t = new Transformer();
            try {
                logger.debug("Simplifying: '{}'", discourseCore.getText());
                CoreContextSentence s = t.simplify(discourseCore.getText());

                // set coreText (assume that there is usually only one core)
                if ((s.getCore() != null) && (s.getCore().size() > 0)) {
                    Tree c = s.getCore().get(0);
                    if (c != null) {
                        text = WordsUtils.wordsToString(c.yieldWords());
                    }
                }

                // add (sentence simplification) contexts
                if (s.getContext() != null) {
                    for (Tree c : s.getContext()) {
                        if (c != null) {
                            sentSimContexts.add(createSContext(WordsUtils.wordsToString(c.yieldWords()), discourseCore.getSentenceIdx()));
                        }
                    }
                }
            } catch (SentenceSimplifyingException e) {
                // nothing
            }

            res = new DCore(text, discourseCore.getSentenceIdx(), discourseCore.getText());

            // add (sentence simplification) context relations
            for (SContext sentSimContext : sentSimContexts) {
                res.addSContext(sentSimContext);
            }

            processedDiscourseCores.put(discourseCore, res);
        }

        return res;
    }

    public List<DCore> simplify(List<DiscourseCore> discourseCores) {
        this.processedDiscourseCores = new LinkedHashMap<DiscourseCore, DCore>();
        this.processedDiscourseContexts = new LinkedHashMap<DiscourseContext, DContext>();

        List<DCore> res = new ArrayList<DCore>();

        for (DiscourseCore discourseCore : discourseCores) {
            DCore dCore = getDCore(discourseCore);

            // add (discourse) core relations
            for (DiscourseCoreCoreRelation discourseCoreCoreRelation : discourseCore.getCoreRelations()) {
                dCore.addDCoreRelation(new DCoreRelation(discourseCoreCoreRelation.getRelation(), getDCore(discourseCoreCoreRelation.getCore())));
            }

            // add (discourse) context relations
            for (DiscourseCoreContextRelation discourseCoreContextRelation : discourseCore.getContextRelations()) {

                // convert into a DContext or a SContext
                if (discourseCoreContextRelation.getContext().isSentSimContext()) {
                    dCore.addSContext(new SContext(discourseCoreContextRelation.getContext().getText(), discourseCoreContextRelation.getContext().getSentenceIdx(), discourseCoreContextRelation.getRelation()));
                } else {
                    dCore.addDContextRelation(new DContextRelation(discourseCoreContextRelation.getRelation(), getDContext(discourseCoreContextRelation.getContext())));
                }
            }

            res.add(dCore);
        }

        return res;
    }
}
