package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthias on 31.12.16.
 */
public class SContext implements PrettyTreePrinter.Node {
    private final String text;
    private final int sentenceIdx;
    private final Relation relation;

    public SContext(String text, int sentenceIdx, Relation relation) {
        this.text = text;
        this.sentenceIdx = sentenceIdx;
        this.relation = relation;
    }

    public String getText() {
        return text;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    public Relation getRelation() {
        return relation;
    }

    @Override
    public List<String> getPTPCaption() {
        return Arrays.asList("'" + text + "'");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        return new ArrayList<PrettyTreePrinter.Edge>();
    }
}
