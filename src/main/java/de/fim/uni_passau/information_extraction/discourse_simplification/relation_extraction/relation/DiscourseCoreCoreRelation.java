package de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element.DiscourseCore;

/**
 * Created by Matthias on 31.12.16.
 */
public class DiscourseCoreCoreRelation {
    private final Relation relation;
    private final DiscourseCore core;

    public DiscourseCoreCoreRelation(Relation relation, DiscourseCore core) {
        this.relation = relation;
        this.core = core;
    }

    public Relation getRelation() {
        return relation;
    }

    public DiscourseCore getCore() {
        return core;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof DiscourseCoreCoreRelation)
                && (((DiscourseCoreCoreRelation) o).relation.equals(relation))
                && (((DiscourseCoreCoreRelation) o).core.equals(core)));
    }
}
