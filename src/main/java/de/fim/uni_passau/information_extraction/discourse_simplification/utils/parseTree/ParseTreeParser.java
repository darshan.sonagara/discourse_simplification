package de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;

import java.io.StringReader;
import java.util.List;

/**
 * Created by Matthias on 13.12.16.
 */
public class ParseTreeParser {

    private static TokenizerFactory<CoreLabel> TOKENIZER_FACTORY = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
    private static LexicalizedParser LEX_PARSER = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
    static {
        LEX_PARSER.setOptionFlags(new String[]{"-outputFormat", "penn,typedDependenciesCollapsed", "-retainTmpSubcategories"});
    }

    public static Tree parse(String text) throws ParseTreeException {
        List<CoreLabel> rawWords = TOKENIZER_FACTORY.getTokenizer(new StringReader(text)).tokenize();
        Tree bestParse = LEX_PARSER.parseTree(rawWords);
        if (bestParse == null) {
            throw new ParseTreeException(text);
        }

        return bestParse;
    }
}
