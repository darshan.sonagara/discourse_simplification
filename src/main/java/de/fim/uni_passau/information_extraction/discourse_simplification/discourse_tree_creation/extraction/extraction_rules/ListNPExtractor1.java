package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.CoordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.utils.ListNPSplitter;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.utils.TregexUtils;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 21.12.16.
 */
public class ListNPExtractor1 extends ExtractionRule {

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: (S < (NP=np < (NP $.. NP) $.. VP))");
        TregexMatcher matcher = p.matcher(parseTree);

        List<TregexUtils.MyMatch> matches = TregexUtils.sortedFindAt(parseTree, p, Arrays.asList("np"));
        if (matches.size() > 0) {
            TregexUtils.MyMatch match = matches.get(0);

            Optional<ListNPSplitter.Result> r = ListNPSplitter.split(match.getNode("np"));
            if (r.isPresent()) {

                // constituents
                List<Word> precedingWords = ParseTreeExtractionUtils.getPrecedingWords(parseTree, match.getNode("np"), false);
                List<Word> followingWords = ParseTreeExtractionUtils.getFollowingWords(parseTree, match.getNode("np"), false);
                List<List<Word>> constituentsWords = new ArrayList<List<Word>>();


                for (List<Word> element : r.get().getElementsWords()) {
                    List<Word> constituentWords = new ArrayList<Word>();

                    constituentWords.addAll(precedingWords);
                    constituentWords.addAll(element);
                    constituentWords.addAll(followingWords);

                    constituentsWords.add(constituentWords);
                }

                // result
                Extraction res = new CoordinationExtraction(
                        getClass().getSimpleName(),
                        r.get().getRelation(),
                        constituentsWords,
                        Leaf.Type.TERMINAL
                );

                return Optional.of(res);
            }
        }


        return Optional.empty();
    }
}
