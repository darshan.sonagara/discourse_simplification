package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class DiscourseTree implements PrettyTreePrinter.Node {
    protected final String extractionRule;
    protected boolean processed;
    protected DiscourseTree parent; //optional
    private int sentenceIdx;

    public DiscourseTree(String extractionRule) {
        this.extractionRule = extractionRule;
        this.processed = false;
        this.parent = null; // should be set by inherited classes
        this.sentenceIdx = -1; // should be set by inherited classes
    }

    public void setRecursiveUnsetSentenceIdx(int sentenceIdx) {
        if (this.sentenceIdx < 0) {
            this.sentenceIdx = sentenceIdx;

            // recursive
            if (this instanceof Coordination) {
                ((Coordination) this).getCoordinations().forEach(c -> c.setRecursiveUnsetSentenceIdx(sentenceIdx));
            }
            if (this instanceof Subordination) {
                ((Subordination) this).getLeftConstituent().setRecursiveUnsetSentenceIdx(sentenceIdx);
                ((Subordination) this).getRightConstituent().setRecursiveUnsetSentenceIdx(sentenceIdx);
            }
        }
    }

    public void cleanup() {
        if (this instanceof Coordination) {

            // remove invalidations
            ((Coordination)this).removeInvalidations();

            // recursion
            ((Coordination)this).getCoordinations().forEach(c -> c.cleanup());
        }

        if (this instanceof Subordination) {

            // recursion
            ((Subordination)this).getLeftConstituent().cleanup();
            ((Subordination)this).getRightConstituent().cleanup();
        }
    }

    public boolean usableAsReference() {
        return  ((parent != null) && (parent instanceof Coordination) && (((Coordination)parent).relation.equals(Relation.UNKNOWN_COORDINATION)));
    }

    public void useAsReference() {
        if (usableAsReference()) {
            ((Coordination)parent).invalidateCoordination(this);
        } else {
            throw new AssertionError("Not useable as reference");
        }
    }

    public List<Leaf> getNucleusPathLeaves() {
        List<Leaf> res = new ArrayList<Leaf>();

        if (this instanceof Leaf) {
            res.add((Leaf) this);
        } else {
            // recursion on coordinations
            if (this instanceof Coordination) {
                for (DiscourseTree child : ((Coordination)this).getCoordinations()) {
                    res.addAll(child.getNucleusPathLeaves());
                }
            }

            // recursion on superordinations
            if (this instanceof Subordination) {
                res.addAll(((Subordination)this).getSuperordination().getNucleusPathLeaves());
            }
        }

        return res;
    }

    public Optional<DiscourseTree> getPreviousNode() {
        if (parent != null) {
            if (parent instanceof Coordination) {
                Coordination p = (Coordination)parent;
                DiscourseTree prev = null;
                for (DiscourseTree child : p.getCoordinations()) {
                    if ((child.equals(this)) && (prev != null)) {
                        return Optional.of(prev);
                    }
                    prev = child;
                }
            }
            if (parent instanceof Subordination) {
                Subordination p = (Subordination)parent;
                if (p.getRightConstituent().equals(this)) {
                    return Optional.of(p.getLeftConstituent());
                }
            }

            // recursion
            return parent.getPreviousNode();
        }

        return Optional.empty();
    }

    public void setProcessed() {
        this.processed = true;
    }

    public boolean isProcessed() {
        return processed;
    }

    public String getExtractionRule() {
        return extractionRule;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    @Override
    public String toString() {
        return PrettyTreePrinter.prettyPrint(this, false);
    }
}
