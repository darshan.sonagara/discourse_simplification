package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element;

import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation.DContextRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation.DCoreRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 08.12.16.
 */
public class DCore implements PrettyTreePrinter.Node {
    private final String text;
    private final int sentenceIdx;
    private final String notSimplifiedText;
    private List<DCoreRelation> dCoreRelations;
    private List<DContextRelation> dContextRelations;
    private List<SContext> sContexts;

    public DCore(String text, int sentenceIdx, String notSimplifiedText) {
        this.text = text;
        this.sentenceIdx = sentenceIdx;
        this.notSimplifiedText = notSimplifiedText;
        this.dCoreRelations = new ArrayList<DCoreRelation>();
        this.dContextRelations = new ArrayList<DContextRelation>();
        this.sContexts = new ArrayList<SContext>();
    }

    public void addDCoreRelation(DCoreRelation dCoreRelation) {
        if (!dCoreRelations.contains(dCoreRelation)) {
            dCoreRelations.add(dCoreRelation);
        }
    }

    public void addDContextRelation(DContextRelation dContextRelation) {
        if (!dContextRelations.contains(dContextRelation)) {
            dContextRelations.add(dContextRelation);
        }
    }

    public void addSContext(SContext sContext) {
        this.sContexts.add(sContext);
    }

    public String getText() {
        return text;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    public String getNotSimplifiedText() {
        return notSimplifiedText;
    }

    public List<DCoreRelation> getDCoreRelations() {
        return dCoreRelations;
    }

    public List<DContextRelation> getDContextRelations() {
        return dContextRelations;
    }

    public List<SContext> getSContexts() {
        return sContexts;
    }


    @Override
    public List<String> getPTPCaption() {
        return Arrays.asList("'" + text + "'");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        List<PrettyTreePrinter.Edge> res = new ArrayList<PrettyTreePrinter.Edge>();

        res.addAll(dCoreRelations.stream().map(
                cr -> new PrettyTreePrinter.DefaultEdge("<d-core:" + cr.getRelation() + ">", cr.getDCore(), false)
        ).collect(Collectors.toList()));

        res.addAll(dContextRelations.stream().map(
                cr -> new PrettyTreePrinter.DefaultEdge("<d-context:" + cr.getRelation() + ">", cr.getDContext(), true)
        ).collect(Collectors.toList()));

        res.addAll(sContexts.stream().map(
                sc -> new PrettyTreePrinter.DefaultEdge("<s-context:" + sc.getRelation() + ">", sc, true)
        ).collect(Collectors.toList()));

        return res;
    }

    @Override
    public String toString() {
        return PrettyTreePrinter.prettyPrint(this, false, 40);
    }
}
