package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Coordination;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.DiscourseTree;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 05.01.17.
 */
public class CoordinationExtraction extends Extraction {
    private final String extractionRule;
    private final Relation relation;
    private final String signalPhrase; // optional
    private final List<String> coordinations;
    private final Leaf.Type coordinationsType;

    public CoordinationExtraction(String extractionRule, Relation relation, List<List<Word>> coordinationsWords, Leaf.Type coordinationsType) {
        this.extractionRule = extractionRule;
        this.relation = relation;
        this.signalPhrase = null;
        this.coordinations = coordinationsWords.stream().map(ws -> WordsUtils.wordsToProperSentenceString(ws)).collect(Collectors.toList());
        this.coordinationsType = coordinationsType;
    }

    // binary
    public CoordinationExtraction(String extractionRule, Relation relation, List<Word> signalPhraseWords, List<Word> leftCoordinationWords, List<Word> rightCoordinationWords, Leaf.Type coordinationsType) {
        this.extractionRule = extractionRule;
        this.relation = relation;
        this.signalPhrase = (signalPhraseWords != null)? WordsUtils.wordsToString(signalPhraseWords) : null;
        this.coordinations = Arrays.asList(leftCoordinationWords, rightCoordinationWords).stream().map(ws -> WordsUtils.wordsToProperSentenceString(ws)).collect(Collectors.toList());
        this.coordinationsType = coordinationsType;
    }

    public DiscourseTree convert() {
        return new Coordination(
                extractionRule,
                relation,
                signalPhrase,
                coordinations.stream().map(
                        s -> new Leaf(coordinationsType, extractionRule, s)
                ).collect(Collectors.toList())
        );
    }

}
