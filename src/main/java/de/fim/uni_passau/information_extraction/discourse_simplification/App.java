package de.fim.uni_passau.information_extraction.discourse_simplification;

import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.processing.Processor;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final Processor PROCESSOR = new Processor();

    public static void main(String[] args) throws IOException {
        List<DCore> cores = PROCESSOR.process(new File("input.txt"), Processor.ProcessingType.WHOLE);
//        List<DCore> cores = PROCESSOR.process("The whole text...", Processor.ProcessingType.WHOLE);
//        List<DCore> cores = PROCESSOR.processWikipediaArticles(Arrays.asList("Barack_Obama"), Processor.ProcessingType.WHOLE);

    }

}
