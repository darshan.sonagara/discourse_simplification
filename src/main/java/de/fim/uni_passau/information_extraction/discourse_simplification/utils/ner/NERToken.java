package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

import edu.stanford.nlp.ling.Word;

/**
 * Created by Matthias on 13.12.16.
 */
public class NERToken {
    protected final int index;
    protected final String text;
    protected final String category;

    public NERToken(int index, String text, String category) {
        this.index = index;
        this.text = text;
        this.category = category;
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public Word getWord() {
        return new Word(text);
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "(" + index + ": " + category +  ", '" + text + "')";
    }
}
