package de.fim.uni_passau.information_extraction.discourse_simplification.utils.words;

import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.Word;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Matthias on 13.12.16.
 */
public class WordsUtils {

    public static String wordsToString(List<Word> words) {
        return Sentence.listToString(words);
    }

    public static String wordsToProperSentenceString(List<Word> words) {
        return wordsToString(wordsToProperSentence(words));
    }

    public static Word capitalizeWord(Word word) {
        String s = word.value();
        if (s.length() > 0) {
            s = s.substring(0, 1).toUpperCase() + s.substring(1);
        }

        return new Word(s);
    }

    public static Word lowercaseWord(Word word) {
        return new Word(word.value().toLowerCase());
    }

    public static List<Word> wordsToProperSentence(List<Word> words) {
        List<Word> res = new ArrayList<Word>();
        res.addAll(words);

        // trim '.' and ',' at beginning and the end and remove multiple, consecutive occurrences
        for (String c : Arrays.asList(".", ",")) {
            Word prev = null;
            Iterator<Word> it = res.iterator();
            while (it.hasNext()) {
                Word word = it.next();
                if (word.value().equals(c)) {
                    if (prev == null || prev.value().equals(word.value())) {
                        it.remove();
                    }
                }
                prev = word;
            }
            if ((!res.isEmpty()) && (res.get(res.size() - 1).value().equals(c))) {
                res.remove(res.size() - 1);
            }
        }

        // add a '.' at the end
        res.add(new Word("."));

        // capitalize first word
        if (!res.isEmpty()) {
            res.set(0, capitalizeWord(res.get(0)));
        }

        return res;
    }
}
