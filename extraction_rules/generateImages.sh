#!/bin/bash
for i in *.dot; do
    dot -Tps "$i" -o "${i%.*}.ps"
done
