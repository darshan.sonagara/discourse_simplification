# DEPRECATED

This repository is no longer maintained, controlled and cannot be commited to.

All development is done on [https://github.com/Lambda-3/DiscourseSimplification](https://github.com/Lambda-3/DiscourseSimplification)


# Discourse Simplification

A project for simplifying sentences wrt. discourse/rhetorical structures.
This works as a wrapper for the `Sentence-Simplification` project.

## Sentence-Simplification Dependency

### use internal maven
When using internal maven repo, nothing further config is needed.

### Clone and install Sentence-Simplification locally
    git clone --branch v3.0-SNAPSHOT https://gitlab.com/ie-pipeline/sentence_simplification.git
    cd sentence_simplification
    mvn install
    
## Clone and install Wiki-Api locally
    git clone -b v1.1 https://gitlab.com/ie-pipeline/wiki-api.git
    cd wiki-api
    mvn install
    
Create the file `wikipedia_tiles.txt` with (all available) wikipedia titles.

## Run the program
    mvn clean compile exec:java

## Make war/jar-file:
    mvn package

## Use as library
    Check App.java